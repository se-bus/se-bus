/**
 * This class stores a reference to a single event callback
 */

import EventStorage from './EventStorage.js';

class EventInstance {
    /**
     * Name of the event
     */
    eventName = null;

    /**
     * Event id
     */
    eventUUID = null;

    /**
     * Constructor
     * @param {string} eventName 
     * @param {string} eventUUID 
     */
    constructor(eventName, eventUUID) {
        this.eventName = eventName;
        this.eventUUID = eventUUID;
    }

    /**
     * This deletes the event
     */
    unregister() {
        EventStorage.remove(this.eventName, this.eventUUID);
    }
}

export default EventInstance;