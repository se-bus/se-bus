/**
 * This is an "event stack"
 * Basicly a bundle of events, that can be deregistered as once
 */

import EventStorage from './EventStorage.js';

class EventStack {
    _thisEvents = [];

    /**
     * Triggers an event once
     * 
     * @param {string} eventName 
     * @param {function} callback 
     * @returns {EventStack}
     */
    once(eventName, callback) {
        this.many(1, eventName, callback);
        return this;
    };

    /**
     * Triggers a callback every time the event is being triggered
     * 
     * @param {string} eventName 
     * @param {function} callback 
     * @returns {EventStack}
     */
    on(eventName, callback) {
        this.many(0, eventName, callback);
        return this;
    }


    /**
     * Triggers a callback {n} time the event is being triggered
     * 
     * @param {number} count
     * @param {string} eventName 
     * @param {function} callback 
     * @returns {EventStack}
     */
    many(count, eventName, callback) {
        this._thisEvents.push(EventStorage.register(count, eventName, callback));
        return this;
    }

    /**
     * Unregisters all events
     */
    unregister() {
        while(this._thisEvents.length >= 1) {
            this._thisEvents.pop().unregister();
        }
    }
}

export default EventStack;