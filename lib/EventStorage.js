/**
 * This is the main class.
 * It stores them, triggers them and deletes them.
 */

import { v4 as uuidv4 } from 'uuid';

import EventInstance from './EventInstance.js';

class EventStorage {
    /**
     * Alle gespeicherten Events
     */
    static events = {};

    /**
     * Registriert ein Event in dem Event-Bus.
     * 
     * @param {number} count 
     * @param {string} eventName 
     * @param {function} callback 
     */
    static register(count, eventName, callback) {
        if (typeof this.events[eventName] == "undefined")
            this.events[eventName] = [];

        const uuid = uuidv4();
        this.events[eventName].push({
            uuid,
            count,
            callback,
        });

        return new EventInstance(
            eventName,
            uuid
        );
    }

    /**
     * Emitted ein Event
     * @param {string} eventName
     * @param {object} parameters
     */
    static execute(eventName, parameters) {
        if (typeof this.events[eventName] == "undefined")
            return;

        var listenersToRemove = [];

        for (var i = 0; i < this.events[eventName].length; i++) {
            this.events[eventName][i].callback(parameters);

            if (this.events[eventName][i].count == 0) continue;
            if (this.events[eventName][i].count == 1) {
                listenersToRemove.push({
                    eventName,
                    uuid: this.events[eventName][i].uuid
                });
            }
            this.events[eventName][i].count--;
        }

        for (var i = 0; i < listenersToRemove.length; i++) {
            this.remove(listenersToRemove[i].eventName, listenersToRemove[i].uuid);
        }
    }

    /**
     * Entfernt ein Event aus dem Speicher
     * @param {string} eventName
     * @param {string} uuid
     */
    static remove(eventName, uuid) {
        if (typeof this.events[eventName] == "undefined")
            return;

        for (var i = 0; i < this.events[eventName].length; i++) {
            if (this.events[eventName][i].uuid != uuid) continue;
            this.events[eventName].splice(i, 1);
            break;
        }

        if (this.events[eventName].length == 0) {
            delete this.events[eventName];
        }
    }
}

export default EventStorage;