import { expect } from 'chai';

import { once } from '../index.js';
import { countObjects, resetObjects } from './00-utils.js';

describe('event registration', function () {
    before(() => { resetObjects(); });

    var event = null;

    it('should add one event', function () {
        event = once('test', () => { });
    });

    it('should now know one event', function () {
        expect(countObjects()).to.equal(1);
    });

    it('should remove the event', function () {
        event.unregister();
    });

    it('should now know no event anymore', function () {
        expect(countObjects()).to.equal(0);
    });

});