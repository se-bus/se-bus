import { expect } from 'chai';

import { on, emit } from '../index.js';
import { resetObjects } from './00-utils.js';

describe('actual callbacks', function () {
    before(() => { resetObjects(); });

    var event = null;
    var triggered = false;

    it('should add one event', function () {
        event = on('test', () => { triggered = true; });
    });

    it('should trigger the event once', function () {
        emit('test', {});
    });

    it('should have been triggered', function () {
        expect(triggered).to.equal(true);
    });
});