import { expect } from 'chai';

import { on, createEventStack, emit } from '../index.js';
import { countObjects, countStackObjects, resetObjects } from './00-utils.js';

describe('event stack registration mixed with regular registration', function () {
    before(() => { resetObjects(); });

    var eventStack = null;

    it('should add one event stack', function () {
        eventStack = createEventStack();
    });

    it('should add two events to the stack', function () {
        eventStack
            .on('test', () => { })
            .once('test2', () => { });
    });

    it('should now have three events', function () {
        expect(countStackObjects(eventStack)).to.equal(2);
    });

    it('should have registered two events', function () {
        expect(countObjects()).to.equal(2);
    });

    it('should register an event without the stack', function () {
        on('test', () => { })
    });

    it('should not have changed stack elements', function () {
        expect(countStackObjects(eventStack)).to.equal(2);
    });

    it('should not have changed elements', function () {
        expect(countObjects()).to.equal(2);
    });

    it('should emit one event', function () {
        emit('test', {});
    });

    it('still know two elements', function () {
        expect(countObjects()).to.equal(2);
    });

    it('should emit second event', function () {
        emit('test2', {});
    });

    it('still know only one element', function () {
        expect(countObjects()).to.equal(1);
    });

    it('should deregister the stack', function () {
        eventStack.unregister();
    });

    it('still know only one element', function () {
        expect(countObjects()).to.equal(1);
    });
});