import { expect } from 'chai';

import { many, emit } from '../index.js';
import { countObjects, resetObjects } from './00-utils.js';

describe('event triggering - many()', function () {
    before(() => { resetObjects(); });

    var event = null;

    it('should add one event', function () {
        event = many(5, 'test', () => { });
    });

    it('should trigger the event once', function () {
        emit('test', {});
    });

    it('should still know the element', function () {
        expect(countObjects()).to.equal(1);
    });

    it('should trigger the event more often', function () {
        for(var i = 0; i < 4; i++) {
            emit('test', {});
        }
    });

    it('should not have any events anymore', function () {
        expect(countObjects()).to.equal(0);
    });
});