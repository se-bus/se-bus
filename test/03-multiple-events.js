import { expect } from 'chai';

import { on } from '../index.js';
import { countObjects, resetObjects } from './00-utils.js';

describe('multiple event registration', function () {
    before(() => { resetObjects(); });

    const eventNames = ['test', 'test2', 'test3', 'test4'];

    var events = [];

    it('should add multiple events', function () {
        for (var i = 0; i < eventNames.length; i++)
            events.push(on(eventNames[i], () => { }));
    });

    it('should now know multiple events', function () {
        expect(countObjects()).to.equal(eventNames.length);
        expect(countObjects()).to.equal(events.length);
    });

    it('should remove one element', function () {
        var ev = events.pop();
        ev.unregister();
    });

    it('should now have removed the element', function () {
        expect(countObjects()).to.not.equal(eventNames.length);
        expect(countObjects()).to.equal(events.length);
    });

    it('should remove all other events', function () {
        while (events.length >= 1) {
            events.pop().unregister();
        }
    });

    it('should now know no element anymore', function () {
        expect(countObjects()).to.equal(0);
    });
});