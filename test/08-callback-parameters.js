import { expect } from 'chai';

import { on, emit } from '../index.js';
import { resetObjects } from './00-utils.js';

describe('actual callbacks with parameters', function () {
    before(() => { resetObjects(); });

    var event = null;
    var parameter = 0;

    it('should add one event', function () {
        event = on('test', (s) => { parameter = s; });
    });

    it('should trigger the event once', function () {
        emit('test', 5);
    });

    it('should have the correct callback', function () {
        expect(parameter).to.equal(5);
    });
});