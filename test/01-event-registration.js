import { expect } from 'chai';

import { once, on, many } from '../index.js';
import { countObjects, resetObjects } from './00-utils.js';

describe('event registration', function () {
    before(() => { resetObjects(); });

    var event = null;

    it('should add one event', function () {
        event = on('test', () => { });
    });


    it('should now know one event', function () {
        expect(countObjects()).to.equal(1);
    });

    it('should add another event', function () {
        const event = once('test2', (s) => { });
        expect(countObjects()).to.equal(2);
    });

    it('should add another event', function () {
        const event = many(2, 'test3', (s) => { });
        expect(countObjects()).to.equal(3);
    });
});