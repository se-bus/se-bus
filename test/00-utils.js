import theStorageClass from '../lib/EventStorage.js';

const countObjects = () => {
    const keys = Object.keys(theStorageClass.events);
    return keys.length;
};

const countStackObjects = (stack) => {
    return stack._thisEvents.length;
};

const resetObjects = () => {
    theStorageClass.events = {};
};

export { 
    countObjects,
    countStackObjects,
    resetObjects
};