import { expect } from 'chai';

import { on, emit } from '../index.js';
import { countObjects, resetObjects } from './00-utils.js';

describe('event triggering - on()', function () {
    before(() => { resetObjects(); });

    var event = null;

    it('should add one event', function () {
        event = on('test', () => { });
    });

    it('should trigger the event once', function () {
        emit('test', {});
    });

    it('should still know the element', function () {
        expect(countObjects()).to.equal(1);
    });

    it('should trigger the event more often', function () {
        for(var i = 0; i < 4; i++) {
            emit('test', {});
        }
    });

    it('should still have the element', function () {
        expect(countObjects()).to.equal(1);
    });
});