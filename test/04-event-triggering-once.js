import { expect } from 'chai';

import { once, emit } from '../index.js';
import { countObjects, resetObjects } from './00-utils.js';

describe('event triggering - once()', function () {
    before(() => { resetObjects(); });

    var event = null;

    it('should add one event', function () {
        event = once('test', () => { });
    });

    it('should trigger a not registered the event', function () {
        emit('test2', {});
    });

    it('still have the first event still registered', function () {
        expect(countObjects()).to.equal(1);
    });

    it('should trigger the registered event', function () {
        emit('test', {});
    });

    it('should not have any events anymore', function () {
        expect(countObjects()).to.equal(0);
    });
});