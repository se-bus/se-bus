import { expect } from 'chai';

import { createEventStack } from '../index.js';
import { countObjects, countStackObjects, resetObjects } from './00-utils.js';

describe('event stack registration', function () {
    before(() => { resetObjects(); });

    var eventStack = null;

    it('should add one event stack', function () {
        eventStack = createEventStack();
    });

    it('should now be an empty event stack', function () {
        expect(countStackObjects(eventStack)).to.equal(0);
    });

    it('should add some events to the stack', function () {
        eventStack
            .on('test', () => { })
            .once('test', () => { })
            .many(4, 'test', () => { });
    });

    it('should now have three events', function () {
        expect(countStackObjects(eventStack)).to.equal(3);
    });

    it('should have registered one event', function () {
        expect(countObjects()).to.equal(1);
    });

    it('should add another event', function () {
        eventStack
            .on('test2', () => { })
    });

    it('should now have four events', function () {
        expect(countStackObjects(eventStack)).to.equal(4);
    });

    it('should have registered two events', function () {
        expect(countObjects()).to.equal(2);
    });

    it('should unregister all callbacks', function () {
        eventStack.unregister();
    });

    it('should now have no events', function () {
        expect(countStackObjects(eventStack)).to.equal(0);
    });

    it('should know no element anymore', function () {
        expect(countObjects()).to.equal(0);
    });
});