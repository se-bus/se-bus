import EventStorage from './lib/EventStorage.js';
import EventStack from './lib/EventStack.js';

/**
 * Triggers an event once
 * 
 * @param {string} eventName 
 * @param {function} callback 
 * @returns 
 */
const once = (eventName, callback) => {
    return many(1, eventName, callback);
};

/**
 * Triggers a callback every time the event is being triggered
 * 
 * @param {string} eventName 
 * @param {function} callback 
 * @returns 
 */
const on = (eventName, callback) => {
    return many(0, eventName, callback);
}


/**
 * Triggers a callback {n} time the event is being triggered
 * 
 * @param {number} count
 * @param {string} eventName 
 * @param {function} callback 
 * @returns 
 */
const many = (count, eventName, callback) => {
    return EventStorage.register(count, eventName, callback);
};

/**
 * Triggers the event and passes a parameter to every callback
 * 
 * @param {string} eventName 
 * @param {object} parameters
 */
const emit = (eventName, parameters) => {
    EventStorage.execute(eventName, parameters);
}

/**
 * Creates a new event stack
 * 
 * @returns {EventStack}
 */
const createEventStack = () => {
    return new EventStack();
}

export {
    once,
    on,
    many,
    emit,
    createEventStack,
}